[![DOI](https://zenodo.org/badge/doi/10.5281/zenodo.4686954.svg)](https://doi.org/10.5281/zenodo.4686954)

# scen_all_scenarios_electr_district_heat_CO2

# CO2 Emission factors: 2015-2050
## Repository structure
```
data                    -- containts the dataset in CSV format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration
```

## Documentation
This repository provides data on scenarios for average CO2 emission factors for electricity generation and district heating grids on country level level (NUTS0). The CO2 emission factor indicates the CO2 intensity, thus the amount of CO2 that is emitted if one MWhe of electricity or heat is generated. All emission factors are given in tCO2/MWhe. The time series provides the emission factors which are averaged over the yearly generation. Values are given in five year steps. 
The given dataset stems from our energy system modelling simulation.  For assessing the future energy system (electricity system & end heating systems, the two models Green-X and Enertile were soft coupled. For details on the interplay and the simulations of the two models, please refer to [Hotmaps WP2 report](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) section 2.3.1 page 24.<br>
The emission factors in this datasets are the results from two representative scenarios to carried out to provide a insights on the future evolution of renewable energies in the electricity sector and in district heating under distinct policy and market trends: <br>
 <ul type="square">
    <li>Scenario A – current policy scenario (scenario name ‘current’ in the dataset), where an overall RES share of 27% has to be achieved at EU level by 2030 under trend electricity market design.</li>
 	<li>Scenario B – ambitious policy scenarios (scenario name ‘ambitious’ in the dataset)  : here the policy ambition is stronger for renewables and for energy efficiency, assuming that the EU aims for a RES share of 33% by 2030 under optimal electricity market design.</li>
</ul>
Key input parameters of the modelled scenarios are derived from PRIMES modelling and from the Green-X database with respect to the potentials and cost of RES technologies. PRIMES comes into play for energy demand developments as well as fossil energy and carbon price trends. The specific PRIMES scenarios used are the latest publicly available reference scenario (European Commission, 2016f) and the climate mitigation scenarios PRIMES euco27 and PRIMES euco30 that build on the targeted use of renewables (i.e. 27% RES by 2030) and an enhanced use of energy efficiency (EE) compared to reference conditions – i.e. 27% (euco27) or 30% EE (euco30) by 2030, respectively. 
For additional information on simulation assumptions and parameters, as well as the two scenarios  see [Hotmaps WP2 report](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) section 3.3.1 page 70ff.  respectively.

### Limitations of the datasets
As with energy system model activities, scenario results do not provide a forecast of the future energy system but provide indications about how it could look like under given parameters and assumptions. In consequence, results and such the present dataset are only as valid as its underlying models. Both Green-X as well as Enertile are well tested and proven models used for a multidude of publications (cf. References and other), yet can only provide a simplified image of the real energy system. 

### References
[1] [European Commission, 2016. SWD(2016) 410 final: Impact assessment. Accompanying the documents: COM(2016) 861 final, SWD(2016) 411 final, SWD(2016) 412 final, SWD(2016) 413 final]( https://ec.europa.eu/energy/sites/ener/files/documents/mdi_impact_assessment_main_report_for_publication.pdf)<br>
[2] Resch G., L. Liebmann, A. Ortner, J. Geipel, M. Welisch, A. Hiesl, C. Zehetner, S. Busch, M. Ragwitz, A. Held, S. Steinhilber, M. Magosch, C. Klessmann, L. Janeiro, L. Tesniere, A. Behrens, A. Hassel, F. Genoese, C. Karakosta, A. Papapostolou, H. Doukas, J. Jansen, K. Veum, J.M. Glachant, A. Henriot, H. Gil, L. Szabo; 2017. Final report of the project towards2030-dialogue. A European research project led by TU Wien, supported by the EASME of the European Commission within the “Intelligent Energy Europe” programme. TU Wien, Energy Economics Group, Vienna, Austria, July 2017. Accessible at www.towards2030.eu.

## How to cite
Lukas Kranzl, Michael Hartner, Andreas Müller, Gustav Resch, Sara Fritz, (TU Wien), Tobias Fleiter, Andrea Herbst, Matthias Rehfeldt, Pia Manz (Fraunhofer ISI), Alyona Zubaryeva (EURAC)
Reviewed by 
Jakob Rager (CREM)
Hotmaps Project, D5.2 WP5 Report – Heating & Cooling outlook until 2050, EU 28, 2018 [www.hotmaps-project.eu](http://www.hotmaps-project.eu/wp-content/uploads/2018/05/D2.3-Hotmaps_FINAL-VERSION_for-upload.pdf) 



## Authors
Jasper Geipel <sup>*</sup>,

<sup>*</sup> [TU Wien, EEG](https://eeg.tuwien.ac.at/)
Institute of Energy Systems and Electrical Drives
Gusshausstrasse 27-29/370
1040 Wien

## License
Copyright © 2016-2018: Jasper Geipel
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.
SPDX-License-Identifier: CC-BY-4.0
License-Text: https://spdx.org/licenses/CC-BY-4.0.html


## Acknowledgement
We would like to convey our deepest appreciation to the Horizon 2020 [Hotmaps Project](http://www.hotmaps-project.eu/) (Grant Agreement number 723677), which provided the funding to carry out the present investigation.
